// react
import React from 'react';
import {TouchableOpacity, StyleSheet, View, Text} from 'react-native';

// library
import AntDesign from 'react-native-vector-icons/AntDesign';
import {moderateScale} from 'react-native-size-matters';
// import {DUMMY} from '../Utils/DummyData';

const HeaderTab = ({
  title,
  page,
  calender = false,
  titleCalender,
  arrow = true,
}) => {
  // const dataKu = DUMMY;
  return (
    <View style={styles.headerContainer}>
      <View style={styles.header}>
        {arrow ? (
          <TouchableOpacity onPress={page} style={styles.iconLeft}>
            <AntDesign name="left" size={20} color="#9098B1" />
          </TouchableOpacity>
        ) : null}

        <View style={styles.textContainer}>
          {calender ? (
            <Text style={styles.textTitle}>{titleCalender}</Text>
          ) : (
            <Text style={styles.textTitle}>{title}</Text>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    marginTop: moderateScale(55),
    marginBottom: moderateScale(30),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconLeft: {
    marginLeft: moderateScale(10),
  },
  textContainer: {
    flex: 1,
    alignItems: 'center',
  },
  textTitle: {
    color: '#F14F6F',
    fontSize: moderateScale(18),
    letterSpacing: 1,
    fontWeight: 'bold',
    marginRight: moderateScale(20),
  },
});

export default HeaderTab;
