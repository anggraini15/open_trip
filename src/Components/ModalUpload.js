import React, {useState} from 'react';
import {View, Text, SafeAreaView, ScrollView, Button} from 'react-native';
import Modal from 'react-native-modal';
import {Input, SearchBar, BottomSheet} from 'react-native-elements';
import {moderateScale} from 'react-native-size-matters';
import {COLORS} from '../Utils/Color';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const MyModal = props => {
  return (
    <Modal coverScreen={true} backdropOpacity={0.3} isVisible={props.muncul}>
      <View>
        <View
          style={{
            backgroundColor: 'white',
            margin: moderateScale(50),
            padding: moderateScale(30),
            borderRadius: 10,
          }}>
          <View
            style={{
              margin: moderateScale(10),
              opacity: 1,
            }}>
            <Button
              title="from camera"
              color={COLORS.red}
              onPress={props.camera}
            />
          </View>
          <View style={{margin: moderateScale(10)}}>
            <Button
              title="from library"
              color={COLORS.red}
              onPress={props.library}
            />
          </View>
          <View style={{margin: moderateScale(10)}}>
            <Button title="cancel" onPress={props.cancel} color={COLORS.red} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default MyModal;
