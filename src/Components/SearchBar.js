import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Searchbar} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import {navigate} from '../Function/nav';
import {COLORS} from '../Utils/Color';

const MySearchBar = props => {
  return (
    <View
      style={props.register ? styles.contregister : styles.searchBarContainer}>
      <Searchbar
        onFocus={() => navigate('Trip')}
        value={props.value ? props.value : null}
        onChangeText={props.change ? props.change : null}
        iconColor={props.red ? COLORS.red : null}
        placeholder={props.name}
        style={
          props.red
            ? styles.redStyle
            : props.register
            ? styles.regstyle
            : styles.searchBar
        }
        inputStyle={props.reg ? styles.regtex : null}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    flex: 1,
    marginTop: moderateScale(55),
    marginBottom: moderateScale(25),
    marginHorizontal: moderateScale(6),
  },
  contregister: {
    marginTop: moderateScale(20),
    marginBottom: moderateScale(25),
    marginHorizontal: moderateScale(6),
  },
  regstyle: {
    borderColor: COLORS.softBlack,
    borderWidth: 1,
  },
  regtex: {
    fontSize: 12,
  },
  searchBar: {
    backgroundColor: 'white',
    borderColor: '#EBF0F3',
    elevation: 10,
  },
  redStyle: {
    borderColor: COLORS.red,
    borderWidth: moderateScale(1),
  },
});

export default MySearchBar;
