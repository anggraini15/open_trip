export const SET_LOADING = 'SET_LOADING';
export const SET_SUCCESS = 'SET_SUCCESS';
export const SET_IS_LOGGED = 'SET_IS_LOGGED';
export const SET_FAILED = 'SET_FAILED';
export const SET_EXPIRED = 'SET_EXPIRED';
export const SET_TIME = 'SET_TIME';
export const SET_TIME_EXP = 'SET_TIME_EXP';

export const actionLoading = payload => {
  return {
    type: SET_LOADING,
    payload,
  };
};

export const actionSuccess = payload => {
  return {
    type: SET_SUCCESS,
    payload,
  };
};

export const actionIsLogged = payload => {
  return {
    type: SET_IS_LOGGED,
    payload,
  };
};

export const actionFailed = payload => {
  return {
    type: SET_FAILED,
    payload,
  };
};
export const actionExpired = payload => {
  return {
    type: SET_EXPIRED,
    payload,
  };
};
export const actionTime = payload => {
  return {
    type: SET_TIME,
    payload,
  };
};
export const actionTimeExp = payload => {
  return {
    type: SET_TIME_EXP,
    payload,
  };
};
