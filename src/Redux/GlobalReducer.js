import {
  SET_IS_LOGGED,
  SET_LOADING,
  SET_SUCCESS,
  SET_FAILED,
  SET_EXPIRED,
  SET_TIME,
  SET_TIME_EXP,
} from './GlobalAction';
import {LOGOUT} from '../Screen/Login/Redux/ActionLogin';

const initialState = {
  loading: false,
  Success: false,
  isLogged: false,
  failed: false,
  expired: true,
  timeLogin: 0,
  timeExp: 0,
};

const reducerGlobal = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };

    case SET_SUCCESS:
      return {
        ...state,
        Success: action.payload,
      };

    case SET_IS_LOGGED:
      return {
        ...state,
        isLogged: action.payload,
      };
    case SET_FAILED:
      return {
        ...state,
        failed: action.payload,
      };
    case LOGOUT:
      return {
        ...state,
        isLogged: false,
        expired: true,
      };
    case SET_EXPIRED:
      return {
        ...state,
        expired: action.payload,
      };
    case SET_TIME:
      return {
        ...state,
        timeLogin: action.payload,
      };
    case SET_TIME_EXP:
      return {
        ...state,
        timeExp: action.payload,
      };

    default:
      return state;
  }
};

export default reducerGlobal;
