import {
  GET_CATEGORY_FAILED,
  SET_CATEGORY,
  SET_DATA_BY_MONTH,
} from './ActionHome';
import {SET_DATA_BY_DATE} from '../../Calender/Redux/actionCalender';
const initialState = {
  data: [],
  message: '',
  monthName: null,
  dateName: null,
};

const reducerHomePage = (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORY:
      return {
        ...state,
        data: action.payload,
        monthName: null,
        dateName: null,
      };
    case GET_CATEGORY_FAILED:
      return {
        ...state,
        message: action.payload,
      };
    case SET_DATA_BY_MONTH:
      return {
        ...state,
        data: action.payload.data,
        monthName: action.payload.monthName,
      };
    case SET_DATA_BY_DATE:
      return {
        ...state,
        data: action.payload.data,
        dateName: action.payload.dateName,
      };
    default:
      return state;
  }
};

export default reducerHomePage;
