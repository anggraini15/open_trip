import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  FlatList,
  Animated,
  TouchableOpacity,
  StatusBar,
  LogBox,
} from 'react-native';
import {DUMMY} from '../../../Utils/DummyData';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {moderateScale} from 'react-native-size-matters';
import Inter from '../../../Components/Inter';
import {COLORS} from '../../../Utils/Color';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {TextInput, RadioButton, IconButton, Button} from 'react-native-paper';
import {BottomSheetModal, BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import MySearchBar from '../../../Components/SearchBar';
import {useDispatch, useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';
import ErrorScreen from '../../../Components/Error';
import {
  actionSuccess,
  actionFailed,
  actionLoading,
} from '../../../Redux/GlobalAction';
import {getDataBank} from '../../Order/Redux/ActionOrder';
import {navigate} from '../../../Function/nav';

const DetailHome = props => {
  const [date, setdate] = useState(new Date());
  const [point, setpoint] = useState(1);
  const [jumlahOrang, setjumlahOrang] = useState(1);

  // state bottom sheet
  const BottomRef = useRef(null);
  const snap = useMemo(() => ['25%', '45%'], []);
  const handlePresentModalPress = useCallback(() => {
    BottomRef.current?.present();
  }, []);

  const dispatch = useDispatch();

  // error
  const failed = useSelector(state => state.Global.failed);

  // data home detail
  const dataReducer = useSelector(state => state.DetailHome?.detail?.tripData);
  const kuota = useSelector(
    state => state.DetailHome?.detail?.quotaTripRemains,
  );

  // data image
  const objFilter = Object.fromEntries(
    Object.entries(dataReducer).filter(([key, value]) => key.includes('pict')),
  );
  const imageData = Object.values(objFilter);

  // data date
  const dateData = dataReducer.trip_date_array[0];
  const arrDate = Object.values(dateData);
  console.log(arrDate, 'arr');
  const tripdate = arrDate.map(e => {
    if (!e) {
      return;
    } else {
      const arr = e.split('/').reverse().join('-');
      return arr;
    }
  });
  // const [date1, date2, date3] = arrDate;
  // const date01 = date1.toString();
  // const date01Trip = moment(date1, true).format('YYYY-MM-DD');
  // const {trip_date_1, trip_date_2, trip_date_3} = dateData;
  console.log(tripdate, 'result');
  // console.log(
  //   moment(trip_date_1).format('YYYY-MM-DD'),
  //   trip_date_2,
  //   trip_date_3,
  // );
  // const arrayDate = Object.values(dateData);
  // const tripDate = arrayDate.map(e => moment(e).format());

  // data activity
  const activity = dataReducer.Activities[0]?.day_time_act;

  // const Data = DUMMY[0].data;

  const ScrollX = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    dispatch(actionSuccess(false));
    dispatch(actionFailed(false));
    dispatch(actionLoading(false));
  }, [dispatch]);

  return failed ? (
    <ErrorScreen />
  ) : (
    <SafeAreaView style={styles.safeView}>
      <BottomSheetModalProvider>
        <ScrollView keyboardShouldPersistTaps="always">
          <StatusBar translucent={true} backgroundColor={'white'} />
          {/* search Bar */}
          <MySearchBar red name="Where You Want To Go" />

          {/* Image */}
          <View style={{marginTop: moderateScale(10)}}>
            <Animated.FlatList
              horizontal
              pagingEnabled
              snapToAlignment="center"
              showsHorizontalScrollIndicator={false}
              snapToInterval={widthPercentageToDP(100)}
              keyExtractor={i => `${i}-pict`}
              initialNumToRender={imageData.length}
              contentContainerStyle={{marginBottom: moderateScale(10)}}
              scrollEventThrottle={16}
              decelerationRate={0}
              onScroll={Animated.event(
                [{nativeEvent: {contentOffset: {x: ScrollX}}}],
                {useNativeDriver: false},
              )}
              data={imageData}
              renderItem={({item, index}) => (
                <FastImage
                  // key={index.toString()}
                  source={{
                    uri: item,
                    priority: FastImage.priority.high,
                    cache: FastImage.cacheControl.immutable,
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                  style={{
                    width: widthPercentageToDP(100),
                    height: heightPercentageToDP(50),
                  }}>
                  <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <View style={styles.arrowImage}>
                      <Ionicons
                        name="chevron-back"
                        size={moderateScale(24)}
                        color="white"
                        style={styles.opacityArrow}
                      />
                    </View>
                  </TouchableOpacity>
                </FastImage>
              )}
            />
          </View>

          <View style={{paddingHorizontal: moderateScale(10)}}>
            {/* title */}

            <Inter
              text={dataReducer.trip_name}
              type="ExtraBold"
              size={moderateScale(24)}
            />

            {/* price & duration */}
            <View style={styles.containerPrice}>
              <Inter
                text={`Rp ${new Intl.NumberFormat(['ban', 'id']).format(
                  dataReducer.price,
                )} `}
                text1="/Orang"
                color={COLORS.red}
                type="Bold"
                size={moderateScale(18)}
                size1={moderateScale(12)}
              />
              <View style={styles.boxDuration}>
                <Inter
                  text={`${dataReducer.duration_trip}Days`}
                  color="white"
                  size={moderateScale(12)}
                />
              </View>
            </View>

            {/* terms */}
            <View style={{width: widthPercentageToDP(55)}}>
              <Inter
                size={moderateScale(12)}
                text={dataReducer.term_and_condition}
                type="Light"
              />
              <View style={{paddingTop: moderateScale(10)}}>
                <Inter
                  type="Light"
                  size={moderateScale(12)}
                  text={`Quota Trip Still ${kuota} Left`}
                />
              </View>
            </View>

            {/* pilih tanggal */}
            <View style={{marginVertical: moderateScale(15)}}>
              <Inter
                size={moderateScale(14)}
                text="Pilih Tanggal Keberangkatan"
              />
              <TextInput
                mode="outlined"
                placeholderTextColor={COLORS.softBlack}
                outlineColor={COLORS.softBlack}
                selectionColor={COLORS.softBlack}
                style={styles.inputDate}
                editable={false}
                left={
                  <TextInput.Icon
                    name="calendar-month-outline"
                    color={COLORS.softBlack}
                    onPress={handlePresentModalPress}
                  />
                }
                // onFocus={handlePresentModalPress}
                value={moment(date).format('DD/MM/YYYY')}
              />
              <BottomSheetModal ref={BottomRef} index={1} snapPoints={snap}>
                <Calendar
                  disableAllTouchEventsForDisabledDays={true}
                  disabledByDefault
                  current={new Date()}
                  minDate={new Date()}
                  theme={{
                    arrowColor: COLORS.black,
                    selectedDayBackgroundColor: COLORS.red,
                    selectedDayTextColor: COLORS.red,
                    selectedDotColor: COLORS.red,
                    todayTextColor: COLORS.red,
                  }}
                  markingType="multi-dot"
                  markedDates={{
                    [date]: {
                      selected: true,
                      disableTouchEvent: true,
                      selectedColor: COLORS.white,
                      selectedTextColor: COLORS.red,
                      dotColor: COLORS.red,
                      disabled: false,
                    },
                    [tripdate[0]]: {
                      selected: true,
                      selectedColor: COLORS.white,
                      selectedTextColor: COLORS.red,
                      marked: true,
                      dotColor: COLORS.red,
                      disabled: false,
                    },
                    [tripdate[1]]: {
                      selected: true,
                      selectedColor: COLORS.white,
                      selectedTextColor: COLORS.red,
                      marked: true,
                      dotColor: COLORS.red,

                      disabled: false,
                    },
                    [tripdate[2]]: {
                      selected: true,
                      selectedColor: COLORS.white,
                      selectedTextColor: COLORS.red,
                      marked: true,
                      dotColor: COLORS.red,

                      disabled: false,
                    },
                  }}
                  onDayPress={day => setdate(day.dateString)}
                />
              </BottomSheetModal>
            </View>

            {/* pilih titik */}
            <View>
              <Inter size={moderateScale(14)} text="Pilih Titik Penjemputan" />
              <RadioButton.Group
                si
                onValueChange={i => setpoint(i)}
                value={point}>
                <View style={styles.radioContainer}>
                  <RadioButton
                    color={COLORS.red}
                    uncheckedColor={COLORS.softBlack}
                    value={1}
                  />
                  <Inter
                    size={moderateScale(12)}
                    color={point === 1 ? COLORS.red : COLORS.softBlack}
                    text="Indomart Point Central Park"
                  />
                </View>

                <View style={styles.radioContainer}>
                  <RadioButton
                    color={COLORS.red}
                    uncheckedColor={COLORS.softBlack}
                    value={2}
                  />
                  <Inter
                    size={moderateScale(12)}
                    color={point === 2 ? COLORS.red : COLORS.softBlack}
                    text="Daytrans Jatiwaringin"
                  />
                </View>
                <View style={styles.radioContainer}>
                  <RadioButton
                    color={COLORS.red}
                    uncheckedColor={COLORS.softBlack}
                    value={3}
                  />
                  <Inter
                    size={moderateScale(12)}
                    color={point === 3 ? COLORS.red : COLORS.softBlack}
                    text="Mall Artha Gading"
                  />
                </View>
              </RadioButton.Group>
            </View>

            {/* detail perjalanan */}
            <View style={{marginVertical: moderateScale(15)}}>
              <Inter size={moderateScale(14)} text="Detail Perjalanan" />
              <FlatList
                data={activity}
                keyExtractor={(item, index) => `${index}`}
                renderItem={({item, index}) => (
                  <View key={index}>
                    <View
                      style={{
                        paddingTop: moderateScale(10),
                        borderBottomWidth: moderateScale(3),
                        borderBottomColor: COLORS.red,
                        width: widthPercentageToDP(15),
                      }}>
                      <Inter
                        type="Bold"
                        color={COLORS.red}
                        text={`Day ${item.day}`}
                      />
                    </View>
                    <View style={styles.perjalananBorder}>
                      <FlatList
                        data={item.activity}
                        keyExtractor={(tem, ndex) => `${ndex}`}
                        renderItem={({item, index}) => (
                          <View key={index} style={styles.timeAndDoContainer}>
                            <View
                              style={{
                                width: widthPercentageToDP(13),
                              }}>
                              <Inter
                                size={moderateScale(12)}
                                text={item.time}
                              />
                            </View>

                            <View
                              style={{
                                width: widthPercentageToDP(82),
                              }}>
                              <Inter size={moderateScale(12)} text={item.do} />
                            </View>
                          </View>
                        )}
                      />
                    </View>
                  </View>
                )}
              />
            </View>

            {/* jumlah */}
            <View>
              <Inter size={moderateScale(14)} text="Jumlah Orang" />
              <View style={styles.jumlahContainer}>
                <IconButton
                  color={jumlahOrang > 1 ? COLORS.red : COLORS.softBlack}
                  icon="minus-circle-outline"
                  size={moderateScale(14)}
                  onPress={() => setjumlahOrang(jumlahOrang - 1)}
                  disabled={jumlahOrang <= 1 ? true : false}
                />
                <Inter size={moderateScale(14)} text={jumlahOrang} />
                <IconButton
                  color={COLORS.red}
                  icon="plus-circle-outline"
                  size={moderateScale(14)}
                  onPress={() => setjumlahOrang(jumlahOrang + 1)}
                />
              </View>
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <View>
              <Inter
                color={COLORS.red}
                color1={COLORS.red}
                text="Total : "
                text1={`Rp. ${new Intl.NumberFormat(['ban', 'id']).format(
                  dataReducer.price * jumlahOrang,
                )}`}
                size={moderateScale(14)}
                size1={moderateScale(14)}
                type="Bold"
                type1="Bold"
              />
              <Inter size={moderateScale(12)} text={`${jumlahOrang} Orang`} />
            </View>
            <Button
              mode="contained"
              color={COLORS.red}
              labelStyle={styles.bottomButtonLabel}
              onPress={() => {
                props.navigation.navigate('Order', {
                  date,
                  jumlahOrang,
                });
                dispatch(getDataBank());
              }}>
              Order Now
            </Button>
          </View>
        </ScrollView>
      </BottomSheetModalProvider>
    </SafeAreaView>
  );
};

export default DetailHome;

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
  },
  arrowImage: {
    borderWidth: 1,
    width: widthPercentageToDP(7),
    borderRadius: moderateScale(50),
    backgroundColor: COLORS.black,
    opacity: 0.7,
    marginTop: moderateScale(50),
    marginLeft: moderateScale(30),
    borderColor: COLORS.black,
    alignItems: 'center',
  },
  opacityArrow: {
    opacity: 1,
  },
  containerPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: moderateScale(10),
  },
  boxDuration: {
    backgroundColor: COLORS.black,
    width: widthPercentageToDP(12),
    alignItems: 'center',
  },
  inputDate: {
    fontSize: moderateScale(14),
    fontFamily: 'Inter',
    height: moderateScale(40),
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  radioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  perjalananBorder: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: moderateScale(1),
    borderTopColor: COLORS.softBlack,
  },
  timeAndDoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: moderateScale(10),
  },
  jumlahContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: widthPercentageToDP(35),
    borderWidth: moderateScale(1),
    justifyContent: 'space-around',
    borderColor: COLORS.softBlack,
    marginVertical: moderateScale(20),
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    elevation: moderateScale(5),
    width: widthPercentageToDP(100),
    backgroundColor: COLORS.white,
    height: heightPercentageToDP(10),
    shadowColor: COLORS.softBlack,
    paddingHorizontal: moderateScale(20),
    borderTopWidth: moderateScale(1),
    borderTopColor: COLORS.softBlack,
  },
  bottomButtonLabel: {
    color: COLORS.white,
    fontSize: moderateScale(12),
    fontFamily: 'Inter',
  },
});
