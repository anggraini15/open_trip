export const GET_DETAIL = 'GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'GET_DETAIL_SUCCESS';
export const GET_DETAIL_ERROR = 'GET_DETAIL_ERROR';
export const SET_DETAIL = 'SET_DETAIL';

export const getDetail = payload => ({type: GET_DETAIL, payload});

export const getDetailSuccess = payload => ({
  type: GET_DETAIL_SUCCESS,
  payload,
});

export const getDetailError = payload => ({type: GET_DETAIL_ERROR, payload});

export const setDetail = payload => ({
  type: SET_DETAIL,
  payload,
});
