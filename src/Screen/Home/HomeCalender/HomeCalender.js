import React, {useEffect} from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  LogBox,
} from 'react-native';
// component
import Home from '../Home';
import HeaderTab from '../../../Components/Header';
import styles from './style';
import Inter from '../../../Components/Inter';
// import {DUMMY} from '../../../Utils/DummyData';
import LoadingAnimated from '../../../Components/Loading';
import {getDetail} from '../HomeDetail/Redux/ActionHomeDetail';
// library
import FastImage from 'react-native-fast-image';
import Feather from 'react-native-vector-icons/Feather';
// redux
import {useDispatch, useSelector} from 'react-redux';

const HomeCalender = props => {
  const backHome = () => {
    props.navigation.navigate(Home);
  };
  // const titles = props.route.params;
  // console.log(props.route);

  const dispatch = useDispatch();

  const DataListCategory = useSelector(
    state => state.ReducerHomeCalender.data.Trips,
  );
  const title = useSelector(state => state.ReducerHomeCalender.data);
  const LoadingData = useSelector(state => state.Global.loading);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, [dispatch]);

  // const myData = DUMMY[0].data;

  return (
    <SafeAreaView style={styles.container}>
      <HeaderTab title={title.category_name} page={backHome} />

      {LoadingData ? (
        <LoadingAnimated />
      ) : (
        <ScrollView style={styles.card}>
          {/* {DataListCategory.map((e, i) => {
            return ( */}

          <FlatList
            data={DataListCategory}
            keyExtractor={items => items.id}
            renderItem={({item, index}) => (
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => dispatch(getDetail(item.id))}>
                <View style={styles.cardContainer}>
                  <FastImage
                    resizeMode={FastImage.resizeMode.cover}
                    source={{
                      uri: item.thumbnail_pict,
                    }}
                    style={styles.cardImage}
                  />

                  <View style={styles.textContainer}>
                    <Inter style={styles.trip} text={item.trip_name} />

                    <Inter
                      style={styles.termss}
                      text={item.term_and_condition}
                      line={3}
                    />
                    <View style={styles.textContainer2}>
                      <Inter style={styles.prices} text={item.price} />
                      <Inter
                        style={styles.durations}
                        text={`${item.duration_trip}Days`}
                      />
                    </View>

                    <View style={styles.textContainer3}>
                      <Feather
                        name="map-pin"
                        size={11}
                        color="#F24B5B"
                        style={styles.pinLoc}
                      />
                      <Inter style={styles.locations} text={item.pick_spot} />
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />

          {/* );
          })} */}
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default HomeCalender;
