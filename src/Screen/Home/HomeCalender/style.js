// react
import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

// library
import {moderateScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: '#f9f9f9',
  },
  cardContainer: {
    backgroundColor: 'white',
    elevation: 1,
    height: heightPercentageToDP(25),
    marginVertical: moderateScale(15),
    marginHorizontal: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  card1: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    // flex: 1,
    width: widthPercentageToDP(80),
    // height: heightPercentageToDP(30),
    // backgroundColor: 'red',
  },
  cardImage: {
    width: moderateScale(120),
    height: moderateScale(184),
    // right: moderateScale(25),
    // top: moderateScale(105),
  },
  viewImage: {
    marginTop: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    width: widthPercentageToDP(40),
    backgroundColor: 'green',
  },
  textContainer: {
    alignItems: 'flex-start',
    // bottom: moderateScale(40),
    right: moderateScale(15),
    // backgroundColor: 'red',
    width: moderateScale(200),
    // top: moderateScale(5),
  },
  trip: {
    fontWeight: 'bold',
    fontSize: moderateScale(14),
    letterSpacing: 0.5,
    paddingTop: moderateScale(20),
    paddingBottom: moderateScale(5),
    // marginBottom: moderateScale(12),
    // marginTop: moderateScale(17),
    color: '#4a4a4a',
  },
  termss: {
    textAlign: 'justify',
    fontSize: moderateScale(12),
    // alignSelf: 'center',
  },
  textContainer2: {
    width: widthPercentageToDP(50),
    paddingVertical: moderateScale(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  prices: {
    // marginTop: moderateScale(22),
    color: '#F24B5B',
    fontWeight: 'bold',
    fontSize: moderateScale(14),
    letterSpacing: 0.5,
  },
  durations: {
    // marginTop: moderateScale(22),
    backgroundColor: '#4A4A4A',
    color: 'white',
    fontSize: moderateScale(11),
    alignSelf: 'center',
    paddingVertical: moderateScale(4),
    paddingHorizontal: moderateScale(6),
  },
  textContainer3: {
    flexDirection: 'row',
    // marginBottom: moderateScale(17),
  },
  pinLoc: {
    // marginTop: moderateScale(24),
  },
  locations: {
    marginTop: moderateScale(20),
    marginLeft: moderateScale(6),
    color: '#b6b6b6',
  },
});

export default styles;
