import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {GET_HOME_CALENDER} from './actionHomeCalender';
import {setHomeCalender} from './actionHomeCalender';
import {actionFailed, actionLoading} from '../../../../Redux/GlobalAction';
import {openTrip} from '../../../../Utils/baseurl';
import {navigate} from '../../../../Function/nav';
import {getDataHomeFailed} from '../../Redux/ActionHome';

function* sagaGetHomeCalender(action) {
  try {
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: `category?id=${action.payload}`,
      method: 'GET',
      validateStatus: status => status < 505,
    });
    console.log(res, 'ISI NYA KE HOME CALENDER');

    if (res.status === 200) {
      yield put(setHomeCalender(res.data.result));
      yield put(actionLoading(false));
      yield navigate('HomeCalender');
    } else {
      yield put(actionFailed(true));
      yield put(getDataHomeFailed(res));
      yield navigate('Home');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getDataHomeFailed(error));
    yield navigate('Home');
    yield put(actionLoading(false));
  }
}

function* sagaHomeCalender() {
  yield takeLatest(GET_HOME_CALENDER, sagaGetHomeCalender);
}

export default sagaHomeCalender;
