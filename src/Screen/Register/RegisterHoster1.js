import React, {useState} from 'react';
import {View, Text, SafeAreaView, ScrollView} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {Input} from 'react-native-elements';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

//import from asset
import LineReg from '../../Assets/Images/LineRegHost.svg';
//import from pages
import styles from './Styles';

//import from utils
import {COLORS} from '../../Utils/Color';
//import from component
import AuthButton from '../../Components/AuthButton';

const RegisterHoster1 = props => {
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [handphone, setHandphone] = useState('');
  const [alamat, setAlamat] = useState('');
  const [message, setMessage] = useState('');
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // const submit = () => {
  //   if (!username) {
  //     setMessage('FullName must be field');
  //   } else if (!password) {
  //     setMessage('password Must be field !!');
  //   } else if (!email) {
  //     setMessage('email Must be field!!');
  //   } else if (!handphone) {
  //     setMessage('Phone No. Must be field!!');
  //   } else if (!alamat) {
  //     setMessage('Alamat Must be field');
  //   } else {
  //     dispatch(
  //       PostNewHoster({
  //         username,
  //         password,
  //         email,
  //         handphone,
  //         alamat,
  //       }),
  //     );
  //   }
  // };

  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <View style={styles.conttitletext}>
            <Text style={styles.hostertitletext}>Register as Hoster</Text>
            <LineReg />
          </View>
          <Text style={styles.steptext}>Step 1/2</Text>
          <View>
            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              onChangeText={text => setUserName(text)}
              placeholder="Username"
              style={{fontSize: 12}}
            />
            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              onChangeText={text => setPassword(text)}
              placeholder="Password"
              style={{fontSize: 12}}
            />
            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              onChangeText={text => setEmail(text)}
              placeholder="Email"
              style={{fontSize: 12}}
            />
            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              onChangeText={text => setHandphone(text)}
              placeholder="No.Handphone"
              style={{fontSize: 12}}
            />
            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              onChangeText={text => setAlamat(text)}
              placeholder="Alamat"
              style={{fontSize: 12}}
            />
            <View style={styles.bottomreg}>
              <AuthButton
                submit={() => props.navigation.navigate('Register')}
                style={{width: moderateScale(150), height: moderateScale(50)}}
                judul="Cancel"
              />
              <AuthButton
                submit={() => props.navigation.navigate('RegisterHoster2')}
                full
                style={{width: moderateScale(150), height: moderateScale(50)}}
                judul="Next Step"
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default RegisterHoster1;
