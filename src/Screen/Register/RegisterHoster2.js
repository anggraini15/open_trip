import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Modal,
  Button,
} from 'react-native';
import {Input, SearchBar, BottomSheet} from 'react-native-elements';
import {moderateScale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Autocomplete from 'react-native-autocomplete-input';
// import BottomSheet from '@gorhom/bottom-sheet';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';

//import from assets
import LineReg from '../../Assets/Images/LineRegHost.svg';
//import from pages
import styles from './Styles';
//import from utils
import {COLORS} from '../../Utils/Color';
//import from component
import AuthButton from '../../Components/AuthButton';
import MyModal from '../../Components/ModalUpload';
import MySearchBar from '../../Components/SearchBar';

const RegisterHoster2 = props => {
  // const [ktp, setKtp] = useState('');
  // const [foto, setFoto] = useState('');
  // const [butab, setButab] = useState('');
  // const [bank, setBank] = useState('');
  // const [rekening, setRekening] = useState('');
  // const [message, setMessage] = useState('');
  // const navigation = useNavigation();
  // const dispatch = useDispatch();

  // const submit = () => {
  //   if (!ktp) {
  //     setMessage('KTP must be uploaded');
  //   } else if (!foto) {
  //     setMessage('Foto Selfie must be uploaded');
  //   } else if (!butab) {
  //     setMessage('Buku Tabungan must be uploaded');
  //   } else if (!bank) {
  //     setMessage('Bank name must field ');
  //   } else if (!rekening) {
  //     setMessage('Nomor Rekening must be field ');
  //   } else {
  //     dispatch(
  //       PostNewHoster({
  //         ktp,
  //         foto,
  //         butab,
  //         bank,
  //         rekening,
  //       }),
  //     );
  //   }
  // };

  // UPLOAD PICTURE================
  const [modalmuncul, setModalmuncul] = useState(false);
  const [datafotoktp, setDatafotoktp] = useState({});
  const [datafotoselfie, setDatafotoselfie] = useState({});
  const [datafotobutab, setDatafotobutab] = useState({});

  console.log(datafotoktp);
  console.log(datafotoselfie);
  console.log(datafotobutab);

  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);
  const _goBack = () => console.log('Went back');
  const _handleSearch = () => console.log('Searching');
  const _handleMore = () => console.log('Shown more');

  // const data = ['BRI', 'BNI', 'BCA', 'Mandiri', 'Bank Mega'];
  // const [bank, setBank] = useState('');

  const dataBanksJson = [
    {
      id: '321',
      name: 'BCA',
    },
    {
      id: '3212',
      name: 'Mandiri',
    },
    {
      id: '3213',
      name: 'BRI',
    },
    {
      id: '3214',
      name: 'Muamalat',
    },
    {
      id: '3215',
      name: 'BNI',
    },
  ];

  const [keyword, setKeyword] = useState(null);
  const isLoading = !dataBanksJson?.length;
  const placeholder = isLoading ? 'Loading data...' : 'Bank Name';
  const [results, setresults] = useState(false);

  const dataBanksFiltered = dataBanksJson.filter(item =>
    item.name.toLowerCase().includes(keyword?.toLowerCase()),
  );

  console.log(keyword);

  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <View style={styles.conttitletext}>
            <Text style={styles.hostertitletext}>Register as Hoster</Text>
            <LineReg />
          </View>
          <Text style={styles.steptext}>Step 2/2</Text>
          <Text style={styles.uploadtext}>Upload Dokumen</Text>
          <Text style={styles.contenttext}>
            Silahkan unggah foto dokumen berikut dan isi informasi yang
            diperlukan (JPG/PDF max 1Mb)
          </Text>
          <View
            style={{
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(40),
              paddingTop: moderateScale(25),
            }}>
            <TouchableOpacity
              onPress={() => {
                setModalmuncul(true);
              }}>
              <View style={styles.contfileupload}>
                <View style={styles.fileicon}>
                  <Icon name="image" size={25} color="grey" />
                </View>
                <View style={styles.filetext}>
                  <Text style={{fontWeight: 'bold'}}>KTP</Text>
                  <Text style={{color: COLORS.softBlack}}>UPLOAD</Text>
                </View>
                <Icon name="chevron-right" size={15} color="grey" />
              </View>
            </TouchableOpacity>
            {/* MODAL --- KTP  */}
            <MyModal
              muncul={modalmuncul}
              cancel={() => {
                setModalmuncul(false);
              }}
              camera={async () => {
                await launchCamera({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotoktp(img);
                  }
                });
              }}
              library={async () => {
                await launchImageLibrary({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotoktp(img);
                  }
                });
              }}
            />

            <TouchableOpacity
              onPress={() => {
                setModalmuncul(true);
              }}>
              <View style={styles.contfileupload}>
                <View style={styles.fileicon}>
                  <Icon name="image" size={25} color="grey" />
                </View>
                <View style={styles.filetext}>
                  <Text style={{fontWeight: 'bold'}}>Foto Selfie</Text>
                  <Text style={{color: COLORS.softBlack}}>UPLOAD</Text>
                </View>
                <Icon name="chevron-right" size={15} color="grey" />
              </View>
            </TouchableOpacity>
            {/* MODAL ---- FOTO SELFIE  */}
            <MyModal
              muncul={modalmuncul}
              cancel={() => {
                setModalmuncul(false);
              }}
              camera={async () => {
                await launchCamera({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotoselfie(img);
                  }
                });
              }}
              library={async () => {
                await launchImageLibrary({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotoselfie(img);
                  }
                });
              }}
            />

            <TouchableOpacity
              onPress={() => {
                setModalmuncul(true);
              }}>
              <View style={styles.contfileupload}>
                <View style={styles.fileicon}>
                  <Icon name="image" size={25} color="grey" />
                </View>
                <View style={styles.filetextbutab}>
                  <Text style={{fontWeight: 'bold'}}>Foto Buku Tabungan</Text>
                  <Text style={{color: COLORS.softBlack}}>UPLOAD</Text>
                </View>
                <Icon name="chevron-right" size={15} color="grey" />
              </View>
            </TouchableOpacity>
            {/* MODAL --- BUTAB  */}
            <MyModal
              muncul={modalmuncul}
              cancel={() => {
                setModalmuncul(false);
              }}
              camera={async () => {
                await launchCamera({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotobutab(img);
                  }
                });
              }}
              library={async () => {
                await launchImageLibrary({mediaType: 'photo'}, res => {
                  console.log('res', res);
                  if (res.didCancel) {
                    return;
                  } else {
                    const img = {
                      ...res.assets[0],
                    };

                    setDatafotobutab(img);
                  }
                });
              }}
            />
          </View>

          <View>
            <Text style={styles.uploadtext}>Akun Bank</Text>
            {/* <MySearchBar register reg name="Nama Bank" /> */}

            <View style={styles.containerautocomplete}>
              <View style={styles.searchSection}>
                <AntDesign
                  name="search1"
                  size={18}
                  color="gray"
                  style={styles.searchIcon}
                />
                <Autocomplete
                  editable={!isLoading}
                  data={dataBanksFiltered}
                  // containerStyle={styles.autocompletecontainerstyle}
                  inputContainerStyle={{borderWidth: 0}}
                  value={keyword}
                  onChangeText={VALUE => setKeyword(VALUE)}
                  placeholder={placeholder}
                  hideResults={results}
                  onBlur={() => setresults(true)}
                  onFocus={() => setresults(false)}
                  flatListProps={{
                    keyboardShouldPersistTaps: 'always',
                    keyExtractor: bank => bank.id,
                    renderItem: ({item: {name}}) => (
                      <TouchableOpacity
                        onPress={() => {
                          setKeyword(name);
                          setresults(true);
                        }}>
                        <Text style={styles.itemText}>{name}</Text>
                      </TouchableOpacity>
                    ),
                  }}
                />
              </View>
            </View>

            <Input
              inputContainerStyle={{
                borderWidth: 1,
                paddingLeft: moderateScale(10),
                borderRadius: 5,
                borderColor: COLORS.softBlack,
              }}
              // onChangeText={text => setRekening(text)}
              placeholder="Nomor Rekening"
              style={{fontSize: 14}}
            />
            <View style={styles.bottomreg}>
              <AuthButton
                submit={() => props.navigation.navigate('RegisterHoster1')}
                style={{width: moderateScale(150), height: moderateScale(50)}}
                judul="Back"
              />
              <AuthButton
                // submit={() => props.navigation.navigate(Register)}
                full
                style={{width: moderateScale(150), height: moderateScale(50)}}
                judul="Register"
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default RegisterHoster2;
