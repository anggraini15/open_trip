import axios from 'axios';
import {ToastAndroid} from 'react-native';
import {put, takeLatest} from 'redux-saga/effects';
import {navigate} from '../../../../Routers/Main';
import {POST_NEW_HOSTER, SetNewHoster} from '../Hoster/ActionHosterRegister';

function* postNewHoster(action) {
  try {
    const res = yield axios.post(
      'https://fp-open-trip.herokuapp.com/api/ot/host/regis',
      requestBody,
    );
    if (res.status === 200) {
      yield put(SetNewHoster(res.data));
      yield navigate('RegisterHoster2');
      yield ToastAndroid.show(res.data.message, ToastAndroid.BOTTOM);
    } else {
      console.log(res.statusText);
    }
  } catch (error) {
    yield ToastAndroid.showWithGravity(
      error.message,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
    yield navigate('Register');
  }
}

export function* SagaHosterRegister() {
  yield takeLatest(POST_NEW_HOSTER, postNewHoster);
}
