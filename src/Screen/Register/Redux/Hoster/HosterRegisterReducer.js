import {SET_NEW_HOSTER} from '../Hoster/ActionHosterRegister';

const initialState = {
  data: [],
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_NEW_HOSTER:
      return {
        ...state,
        data: payload,
      };

    default:
      return state;
  }
};
