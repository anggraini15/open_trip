import {put, takeLatest} from 'redux-saga/effects';
import {navigate} from '../../../../Function/nav';
import {
  actionFailed,
  actionLoading,
  actionSuccess,
} from '../../../../Redux/GlobalAction';
import {openTrip} from '../../../../Utils/baseurl';
import {
  POST_NEW_USER,
  SetNewUserFailed,
  SetNewUserSuccess,
} from '../User/ActionUserRegister';

function* postNewUser(action) {
  try {
    // console.log('apaaja');
    yield put(actionLoading(true));
    const {username, password, email} = action.payload;

    const body = {
      username: username,
      email: email,
      password: password,
    };
    // console.log(body);
    // const formData = new URLSearchParams();
    // formData.append('email', email);
    // formData.append('username', username);
    // formData.append('password', password);

    // const res = yield axios.post(url + 'travel/regis', body, {
    //   validateStatus: status => status < 500,
    // });

    const res = yield openTrip({
      url: 'travel/regis',
      method: 'POST',
      data: body,
    });

    if (res.status === 201) {
      yield put(actionSuccess(true));
      yield put(SetNewUserSuccess(res.data));
      yield navigate('LoginScreen');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(SetNewUserFailed(error.response.data.message));
    yield put(actionLoading(false));
  }
}

export function* SagaUserRegister() {
  yield takeLatest(POST_NEW_USER, postNewUser);
}
