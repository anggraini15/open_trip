export const POST_NEW_USER = 'POST_NEW_USER';
export const SET_NEW_USER_SUCCESS = 'SET_NEW_USER_SUCCESS';
export const SET_NEW_USER_FAILED = 'SET_NEW_USER_FAILED';

export const PostNewUser = payload => ({type: POST_NEW_USER, payload});
export const SetNewUserSuccess = payload => ({
  type: SET_NEW_USER_SUCCESS,
  payload,
});
export const SetNewUserFailed = payload => ({
  type: SET_NEW_USER_FAILED,
  payload,
});
