import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  StyleSheet,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import Logo from '../../Components/Logo';
import {Input, Avatar} from 'react-native-elements';
import AuthButton from '../../Components/AuthButton';
import styles from './styles';
import Icon from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {COLORS} from '../../Utils/Color';
import MyModal from '../../Components/ModalUpload';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Profile from './Profile';
import {useDispatch, useSelector} from 'react-redux';
import {PutProfile, setPhoto} from './Redux/ActionProfile';
import {PermissionsAndroid} from 'react-native';

const EditProfile = props => {
  const editData = props.route.params;
  // console.log(editData);

  const fotoEdit = useSelector(state => state.Profile.foto);
  const userdata = useSelector(state => state.Login?.userData);
  const token = useSelector(state => state.Login.data.token_traveller);
  const dispatch = useDispatch();

  const [username, setUserName] = useState(editData.username);
  const [email, setEmail] = useState(editData.email);
  const [password, setPassword] = useState('');

  const [modalmuncul, setModalmuncul] = useState(false);
  const [fotoprofile, setPhotoProfile] = useState(null);

  // console.log(fotoprofile, 'data foto camera');

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{alignItems: 'center', marginBottom: moderateScale(25)}}>
          <TouchableOpacity onPress={() => {}}>
            <View
              style={{
                height: 100,
                width: 100,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setModalmuncul(true);
                }}>
                <ImageBackground
                  onPress={() => {
                    setModalmuncul(true);
                  }}
                  source={
                    fotoprofile
                      ? {uri: fotoprofile.uri}
                      : fotoEdit
                      ? {uri: fotoEdit.uri}
                      : require('../../Assets/Images/luffy.jpg')
                  }
                  style={{height: 100, width: 100}}
                  imageStyle={{borderRadius: 15}}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      name="camera"
                      size={25}
                      color="#fff"
                      style={{
                        opacity: 0.7,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#fff',
                        borderRadius: 10,
                      }}
                    />
                  </View>
                </ImageBackground>
              </TouchableOpacity>
              <MyModal
                muncul={modalmuncul}
                cancel={() => {
                  setModalmuncul(false);
                }}
                camera={async () => {
                  await launchCamera({mediaType: 'photo'}, async res => {
                    console.log('resul camera', res);

                    const granted = await PermissionsAndroid.request(
                      PermissionsAndroid.PERMISSIONS.CAMERA,
                      {
                        title: 'Cool Photo App Camera Permission',
                        message:
                          'Cool Photo App needs access to your camera ' +
                          'so you can take awesome pictures.',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                      },
                    );

                    if (res.didCancel) {
                      return;
                    } else if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
                      return;
                    } else {
                      const img = {
                        ...res.assets[0],
                      };

                      setPhotoProfile(img);
                    }
                  });
                }}
                library={async () => {
                  await launchImageLibrary({mediaType: 'photo'}, res => {
                    if (res.didCancel) {
                      return;
                    } else {
                      const img = {
                        ...res.assets[0],
                      };

                      setPhotoProfile(img);
                    }
                  });
                }}
              />
            </View>
          </TouchableOpacity>
          <View>
            <Text
              style={{
                fontWeight: 'bold',
                color: '#fff',
                marginTop: moderateScale(10),
                fontSize: 18,
              }}>
              Change Your Avatar
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.footer}>
        <View
          style={{
            justifyContent: 'space-between',
            paddingHorizontal: moderateScale(2),
            paddingTop: moderateScale(25),
          }}>
          <View>
            <View style={styles.contuserreg}>
              <Input
                defaultValue={username}
                containerStyle={{height: heightPercentageToDP(10)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setUserName(text)}
                placeholder="Full Name"
                leftIcon={<Icon name="user" size={20} color="grey" />}
                style={{fontSize: 13}}
              />

              <Input
                disabled
                defaultValue={email}
                containerStyle={{height: heightPercentageToDP(10)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setEmail(text)}
                placeholder="Your Email"
                leftIcon={<Icon name="mail" size={20} color="grey" />}
                style={{fontSize: 13}}
              />

              <Input
                containerStyle={{height: heightPercentageToDP(10)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setPassword(text)}
                placeholder="Password"
                secureTextEntry
                leftIcon={<Icon name="lock1" fontSize size={20} color="grey" />}
                style={{fontSize: 13}}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.bottomreg}>
            <AuthButton
              submit={() => {
                dispatch(setPhoto(fotoprofile));
                dispatch(
                  PutProfile({
                    id: userdata.user_id,
                    username,
                    password,
                    email,
                    token,
                  }),
                );
              }}
              // style={{width: moderateScale(150), height: moderateScale(50)}}
              full
              judul="Save"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default EditProfile;
