import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/AntDesign';
import {Input} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {Snackbar, HelperText} from 'react-native-paper';
//import from utils
import {COLORS} from '../../Utils/Color';
//import from component
import AuthButton from '../../Components/AuthButton';
import Logo from '../../Components/Logo';

//import from pages
import styles from './styles';
import Register from '../../Screen/Register/Register';
import RegisterUser from '../../Screen/Register/RegisterUser';

import {loginAction, loginFailed} from '../Login/Redux/ActionLogin';
import LoadingAnimated from '../../Components/Loading';
import {actionFailed, actionSuccess} from '../../Redux/GlobalAction';
import {ScrollView} from 'react-native-gesture-handler';
import {heightPercentageToDP} from 'react-native-responsive-screen';

const Login = props => {
  // SEMENTARA
  // const keHome = () => {
  //   props.navigation.navigate('Main');
  // };
  // const keReg = () => {
  //   props.navigation.navigate(Register);
  // };

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [Message, setMessage] = useState('');
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const hasErrors = (check, keyword) => {
    let arr = check.split('');
    if (arr.length === 0) {
      return false;
    } else if (arr.includes(keyword)) {
      return false;
    } else {
      return true;
    }
  };

  const dataFailed = useSelector(state => state.Login?.message);
  const dataSuccess = useSelector(state => state.UserRegister.message);
  const failed = useSelector(state => state.Global.failed);
  const success = useSelector(state => state.Global.Success);
  const loading = useSelector(state => state.Global.loading);

  const submit = () => {
    if (!email) {
      setMessage('Email Must be field !!');
    } else if (!password) {
      setMessage('Password Must be field!!');
    } else {
      dispatch(
        loginAction({
          email,
          password,
        }),
      );
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {loading ? (
        <LoadingAnimated />
      ) : (
        <ScrollView>
          <View>
            <View>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Logo />
                <Text style={styles.subtitlepagetext}>
                  Welcome to Open Trip
                </Text>
                <Text style={styles.subtitlesignin}>Sign in to Continue</Text>
              </View>
              <View>
                <Input
                  containerStyle={{
                    height: heightPercentageToDP(7),
                  }}
                  inputContainerStyle={{
                    borderWidth: 1,
                    paddingLeft: moderateScale(15),
                    borderRadius: 5,
                    borderColor: COLORS.softBlack,
                  }}
                  onChangeText={text => setEmail(text)}
                  placeholder="Email"
                  leftIcon={<Icon name="mail" size={20} color="grey" />}
                  style={{fontSize: 16}}
                />
                <HelperText type="error" visible={hasErrors(email, '@')}>
                  email address is invalid
                </HelperText>
                <Input
                  inputContainerStyle={{
                    borderWidth: 1,
                    paddingLeft: moderateScale(15),
                    borderRadius: 5,
                    borderColor: COLORS.softBlack,
                  }}
                  onChangeText={text => setPassword(text)}
                  placeholder="Password"
                  secureTextEntry
                  leftIcon={<Icon name="lock1" size={20} color="grey" />}
                  style={{fontSize: 16}}
                />

                <AuthButton center submit={submit} full judul="Sign In" />

                <View>
                  <TouchableOpacity>
                    <Text
                      style={styles.forgotpass}
                      onPress={() =>
                        props.navigation.navigate('ForgotPassword')
                      }>
                      Forgot Password?
                    </Text>
                  </TouchableOpacity>
                  <View style={styles.textreg}>
                    <Text style={styles.subtitlesignin}>
                      Don't have account?
                    </Text>
                    <TouchableOpacity
                      onPress={() => props.navigation.navigate('RegisterUser')}>
                      <Text style={styles.reg}>Register</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      )}
      <Snackbar
        style={{backgroundColor: COLORS.red}}
        visible={failed ? failed : success ? success : false}
        onDismiss={() => {
          dispatch(actionFailed(false));
          dispatch(actionSuccess(false));
          dispatch(loginFailed(''));
        }}
        duration={2000}>
        {failed ? dataFailed : dataSuccess}
      </Snackbar>
    </SafeAreaView>
  );
};

export default Login;
