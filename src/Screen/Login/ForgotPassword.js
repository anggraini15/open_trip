import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/AntDesign';
import {Input} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {Snackbar, HelperText} from 'react-native-paper';
//import from utils
import {COLORS} from '../../Utils/Color';
//import from component
import AuthButton from '../../Components/AuthButton';
import Logo from '../../Components/Logo';

//import from pages
import styles from './styles';

import {ScrollView} from 'react-native-gesture-handler';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import Inter from '../../Components/Inter';

const ForgotPassword = props => {
  const loading = useSelector(state => state.Global.loading);

  const [email, setEmail] = useState('');

  const hasErrors = (check, keyword) => {
    let arr = check.split('');
    if (arr.length === 0) {
      return false;
    } else if (arr.includes(keyword)) {
      return false;
    } else {
      return true;
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {loading ? (
        <LoadingAnimated />
      ) : (
        <ScrollView>
          {/* <StatusBar barStyle="dark-content" backgroundColor={COLORS.red} /> */}
          <View>
            <View>
              <Logo />
              <Text style={styles.subtitlepagetext}>Forgot Your Password</Text>
              <View
                style={{
                  alignContent: 'center',
                  alignItems: 'center',
                  marginHorizontal: moderateScale(10),
                  marginBottom: moderateScale(25),
                }}>
                <Inter
                  size={moderateScale(12)}
                  color={COLORS.softBlack}
                  text="Enter the email address used to create your Open Trip account and we'll send you a link ot reset your password"
                />
              </View>
              <View>
                <Input
                  containerStyle={{height: heightPercentageToDP(8)}}
                  inputContainerStyle={{
                    borderWidth: 1,
                    paddingLeft: moderateScale(15),
                    borderRadius: 5,
                    borderColor: COLORS.softBlack,
                  }}
                  //   onChangeText={text => setEmail(text)}
                  placeholder="email"
                  leftIcon={<Icon name="mail" size={20} color="grey" />}
                  style={{fontSize: 16}}
                />
                <HelperText type="error" visible={hasErrors(email, '@')}>
                  email address is invalid
                </HelperText>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <AuthButton full judul="Send Verification Via Email" />
                  <View>
                    <Text
                      onPress={() => props.navigation.navigate('LoginScreen')}
                      style={styles.forgotpass}>
                      back to Login
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default ForgotPassword;
