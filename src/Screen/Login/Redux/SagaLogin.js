import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {navigate} from '../../../Function/nav';
import {LOGIN, setDataLogin, loginFailed} from '../Redux/ActionLogin';
import {url} from '../../../Utils/baseurl';
import {
  actionLoading,
  actionFailed,
  actionSuccess,
  actionIsLogged,
  actionExpired,
  actionTime,
  actionTimeExp,
} from '../../../Redux/GlobalAction';
import moment from 'moment';

function* login(action) {
  try {
    yield put(actionLoading(true));

    const res = yield axios.post(url + 'user/login', action.payload);
    console.log(res, 'Login ');

    if (res.status === 200) {
      yield put(actionSuccess(true));
      yield put(setDataLogin(res.data.data));
      yield put(actionIsLogged(true));
      yield put(actionExpired(false));
      yield put(actionTime(moment().format()));
      yield put(actionTimeExp(moment().add(12, 'hours').format()));
      yield navigate('Main');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(
      loginFailed(error.response.data.Message || error.response.data.message),
    );
    yield navigate('LoginScreen');
    yield put(actionLoading(false));
  }
}

export function* SagaLogin() {
  yield takeLatest(LOGIN, login);
}
