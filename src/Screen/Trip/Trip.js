import React, {useState, useEffect, useCallback} from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  LogBox,
  RefreshControl,
} from 'react-native';
import MySearchBar from '../../Components/SearchBar';
import FastImage from 'react-native-fast-image';
import styles from './style';
import HomeButton from '../../Components/HomeButton';
// import {DUMMY} from '../../Utils/DummyData';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalSwipeable from '../../Components/Modal';
import icons from '../../Assets/Images/iconFilter.png';
import {Calendar} from 'react-native-calendars';
import {COLORS} from '../../Utils/Color';
import {useDispatch, useSelector} from 'react-redux';
import LoadingAnimated from '../../Components/Loading';
import {
  getBudget,
  getDateTrip,
  getDuration,
  getOneMonth,
  getSearchName,
  getSearchTrip,
  getTema,
  setDateTrip,
} from './Redux/ActionSearchTrip';
import moment from 'moment';
import Inter from '../../Components/Inter';
import ErrorScreen from '../../Components/Error';
import {getDetail} from '../Home/HomeDetail/Redux/ActionHomeDetail';
import {moderateScale} from 'react-native-size-matters';
import {TextInput} from 'react-native-paper';

const Trip = props => {
  const [addFilterSection, setFilterSection] = useState(false);
  // const [date, setDate] = useState(moment().format('DD-MM-YYYY'));
  const [date, setdate] = useState(new Date());
  const [tripDate, SetTripDate] = useState('');
  console.log(tripDate, 'DATE!');
  const [press, setPress] = useState(false);
  const [day, setday] = useState(moment().daysInMonth());
  const [month, setmonth] = useState(moment().month() + 1);
  const [year, setyear] = useState(moment().year());

  console.log(day, month, year, 'data month');

  // const selectDates = day => {
  //   let selectedDate = day.dateString;
  //   let newDates = date;
  //   if (date[selectedDate]) {
  //     delete newDates[selectedDate];
  //   } else {
  //     newDates[selectedDate] = {
  //       selected: true,
  //       disableTouchEvent: true,
  //       selectedColor: COLORS.white,
  //       selectedTextColor: COLORS.black,
  //       selectedDotColor: COLORS.red,
  //       marked: true,
  //       activeOpacity: 0,
  //     };
  //   }
  //   setDate({...newDates});
  // };

  const toggleCal = () => {
    setPress(!press);
  };
  const toggleModal = () => {
    setFilterSection(!addFilterSection);
    setPress(false);
  };

  const dispatch = useDispatch();

  const DataSearchTrip = useSelector(state => state.ReducerSearchTrip?.data);
  //   if (state.ReducerSearchTrip.data) {
  //     return state.ReducerSearchTrip.data;
  //   } else {
  //     return state.ReducerSearchTrip?.data[0].Trips;
  //   }
  // });
  // const [data, setData] = useState(null);
  // // console.log(data, 'data dari reducer');
  // const filterData = arr => {
  //   if (arr.length === 0) {
  //     setData(null);
  //   } else {
  //     setData(arr[0].Trips);
  //   }
  // };

  // filterData(DataSearchTrip);

  // useEffect(() => {
  //   if (DataSearchTrip) {
  //     return DataSearchTrip;
  //   }
  // }, [DataSearchTrip]);

  // const dataFiltered =
  //   DataSearchTrip?.length === 10
  //     ? DataSearchTrip
  //     : DataSearchTrip?.length < 1
  //     ? null
  //     : DataSearchTrip[0].Trips.length === 0
  //     ? null
  //     : DataSearchTrip[0].Trips;

  // console.log(dataFiltered, 'data filter');

  // const newData = DataSearchTrip?.map(e => {
  //   return e?.Trips;
  // });
  console.log(DataSearchTrip, 'INI TIMPA DATA DEWI');
  // console.log(newData, 'INI LIAT DEWI');

  const LoadingData = useSelector(state => state.Global.loading);
  const [color, setColor] = useState(false);
  // const DataTema = useSelector(state => state.ReducerSearchTrip?.data[0].Trips);
  // const DataBudget = useSelector(state => state.ReducerSearchTrip?.data);
  // console.log(DataTema, 'DATA TEMA');

  // const loginMessage = useSelector(state => state.Login.message);
  // const success = useSelector(state => state.Global.Success);

  useEffect(() => {
    dispatch(getSearchTrip());
  }, [dispatch]);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

    if (date) {
      setPress(false);
    }
  }, [date]);

  const [senddata, SetSendData] = useState({
    tipe: '',
    tema: '',
    budget: '',
    duration: '',
  });

  // const [Tipe, SetTipe] = useState([
  //   {StatusName: 'Open Trip', isActive: false},
  //   {StatusName: 'Private Trip', isActive: false},
  // ]);

  const [Tema, SetTema] = useState([
    {StatusName: 'Populer', isActive: false, value: 'Destinasi Populer'},
    {StatusName: 'Vitamin Sea', isActive: false, value: 'Vitamin Sea'},
    {
      StatusName: 'Menyatu Dengan Alam',
      isActive: false,
      value: 'Menyatu Dengan Alam',
    },
    {
      StatusName: 'Naik Naik Ke Puncak Gunung',
      isActive: false,
      value: 'Naik-Naik Ke Puncak Gunung',
    },
  ]);

  const [Budget, SetBudget] = useState([
    {StatusName: '3-5 jt', isActive: false, value: []},
    {StatusName: '8-10 jt', isActive: false, value: []},
  ]);

  const [Durations, SetDuration] = useState([
    {StatusName: 'Duration', value: []},
  ]);

  const [Minimum, SetMinimum] = useState('');
  const [Maksimum, SetMaksimum] = useState('');

  const [input, setInput] = useState('');
  // list button
  const listButton = ['Bulan ini'];
  // console.log(listButton, 'INI LIST BUTTON');
  // const myData = DUMMY[0].data;

  const submitBudget = () => {
    dispatch(
      getBudget({
        min: Minimum,
        max: Maksimum,
        page: 0,
        limit: 2,
      }),
    );
  };

  const submitDuration = () => {
    dispatch(
      getDuration({
        min: Minimum,
        max: Maksimum,
        page: 0,
        limit: 2,
      }),
    );
  };

  // data date
  // const dateData = DataSearchTrip?.[0].trip_date_array?.[0];
  // const arrDate = Object.values(dateData);
  // // const [data1, setData1] = useState(arrDate[0]);
  // console.log(arrDate, 'ARR DEWI');
  // const tripdate = arrDate.map(e => {
  //   if (!e) {
  //     return;
  //   } else {
  //     const arr = e.split('/').reverse().join('-');
  //     return arr;
  //   }
  // });
  // console.log(dateData, 'LIAT DATE DEWI');

  // const [ambilDate, SetAmbilDate] = useState('');

  const [refres, setrefres] = useState(false);
  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };
  const Refresh = useCallback(() => {
    setrefres(true);
    dispatch(getSearchTrip());
    wait(2000).then(() => setrefres(false));
  }, []);

  return (
    <SafeAreaView style={styles.fullscreen}>
      {/* SEARCH BAR */}
      <View style={styles.container}>
        {/* <FlatList
          data={DataSearchTrip}
          keyExtractor={item => item.id}
          renderItem={({item}) => ( */}

        <TouchableOpacity
          onPress={() => dispatch(getSearchName())}
          style={{
            justifyContent: 'center',
            width: moderateScale(375),
            height: moderateScale(85),
            marginBottom: moderateScale(35),
          }}>
          <MySearchBar
            red
            name="Where you want to go"
            change={async text => {
              await setInput(text);
              dispatch(getSearchName(input));
            }}
            value={input}
          />
        </TouchableOpacity>

        {/* )}
        /> */}
      </View>

      <View style={styles.backgroundHome}>
        {/* BUTTON */}
        <View style={styles.buttonContainer}>
          <ScrollView horizontal={true}>
            <View style={styles.button}>
              <TouchableOpacity onPress={toggleModal}>
                <HomeButton
                  filter="Filter"
                  style={styles.buttonList}
                  warna={'#9f9f9f'}
                  text={'#9f9f9f'}
                />
                <FastImage
                  style={styles.Logo}
                  source={icons}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </TouchableOpacity>
            </View>
            {/* <Calendar
              // firstDay={1}
              // theme={{
              //   arrowColor: COLORS.black,
              //   selectedDayBackgroundColor: COLORS.red,
              //   selectedDayTextColor: COLORS.red,
              //   selectedDotColor: COLORS.red,
              //   todayTextColor: COLORS.black,
              // }}
              // markedDates={{
              //   [date]: {
              //     selected: true,
              //     disableTouchEvent: true,
              //     selectedColor: COLORS.white,
              //     selectedTextColor: COLORS.red,
              //     selectedDotColor: COLORS.red,
              //   },
              // }}
              disableAllTouchEventsForDisabledDays={true}
              disabledByDefault
              current={new Date()}
              minDate={new Date()}
              theme={{
                arrowColor: COLORS.black,
                selectedDayBackgroundColor: COLORS.red,
                selectedDayTextColor: COLORS.red,
                selectedDotColor: COLORS.red,
                todayTextColor: COLORS.red,
              }}
              markingType="multi-dot"
              markedDates={{
                [date]: {
                  selected: true,
                  disableTouchEvent: true,
                  selectedColor: COLORS.white,
                  selectedTextColor: COLORS.red,
                  dotColor: COLORS.red,
                  disabled: false,
                },
                [tripdate[0]]: {
                  selected: true,
                  selectedColor: COLORS.white,
                  selectedTextColor: COLORS.red,
                  marked: true,
                  dotColor: COLORS.red,
                  disabled: false,
                },
                [tripdate[1]]: {
                  selected: true,
                  selectedColor: COLORS.white,
                  selectedTextColor: COLORS.red,
                  marked: true,
                  dotColor: COLORS.red,

                  disabled: false,
                },
                [tripdate[2]]: {
                  selected: true,
                  selectedColor: COLORS.white,
                  selectedTextColor: COLORS.red,
                  marked: true,
                  dotColor: COLORS.red,

                  disabled: false,
                },
              }}
              // onDayPress={day => {
              // console.log(day, 'INI LIAT DAY NYA');
              // setDate(moment(day.dateString).format('DD-MM-YYYY'));
              onDayPress={day => setdate(day.dateString)}
              // }}
            /> */}
            <ModalSwipeable
              isVisible={addFilterSection}
              onSwipeComplete={x => setFilterSection(false)}
              onBackButtonPress={toggleModal}
              onClose={toggleModal}
              onBackdropPress={toggleModal}
              scrollOffset={0}
              scrollOffsetMax={100}>
              {press ? (
                <View>
                  <Inter style={styles.filterTitle} text="Pilih Tanggal" />
                </View>
              ) : (
                <View>
                  <Inter style={styles.filterTitle} text="Filter" />
                  <ScrollView>
                    {/* <Inter style={styles.tipeTitle} text="Tipe Perjalanan" /> */}
                    {/* <View style={styles.tipePerjalananStyle}> */}
                    {/* {Tipe.map((v, i) => { */}
                    {/* return ( */}
                    {/* <View key={i} style={styles.button1}> */}
                    {/* <TouchableOpacity */}
                    {/* onFocus={() => setColor(true)} */}
                    {/* onBlur={() => setColor(false)} */}
                    {/* onPress={() => { */}
                    {/* SetTipe(prevState => { */}
                    {/* const newArray = prevState.map(value => { */}
                    {/* value.isActive = false; */}
                    {/* return value; */}
                    {/* }); */}
                    {/* newArray[i].isActive = true; */}

                    {/* return [...newArray]; */}
                    {/* }); */}

                    {/* SetSendData({ */}
                    {/* ...senddata, */}
                    {/* tipe: Tipe[i].StatusName, */}
                    {/* }); */}
                    {/* }}> */}
                    {/* <Inter */}
                    {/* style={{ */}
                    {/* ...styles.buttonOpenTrip, */}
                    {/* borderColor: v.isActive */}
                    {/* ? '#F24B5B' */}
                    {/* : '#929292', */}
                    {/* }} */}
                    {/* color={v.isActive ? '#F24B5B' : '#929292'} */}
                    {/* text={Tipe[i].StatusName} */}
                    {/* /> */}
                    {/* </TouchableOpacity> */}
                    {/* <TouchableOpacity>
                              <Inter
                                style={styles.buttonPrivate}
                                text="Private Trip"
                              />
                            </TouchableOpacity> */}
                    {/* </View> */}
                    {/* ); */}
                    {/* })} */}
                    {/* </View> */}

                    <Inter style={styles.temaTitle} text="Tema Perjalanan" />
                    <View style={styles.temaPerjalananStyle}>
                      {Tema.map((v, i) => {
                        return (
                          <View key={i} style={styles.button2}>
                            <TouchableOpacity
                              onFocus={() => setColor(true)}
                              onBlur={() => setColor(false)}
                              onPress={() => {
                                SetTema(prevState => {
                                  const newArray = prevState.map(value => {
                                    value.isActive = false;
                                    return value;
                                  });
                                  newArray[i].isActive = true;

                                  return [...newArray];
                                });
                                dispatch(getTema(v.value));

                                // SetFilterTema(...Tema[i].StatusName);

                                SetSendData({
                                  ...senddata,
                                  tipe: Tema[i].StatusName,
                                });
                              }}>
                              <Inter
                                style={{
                                  ...styles.buttonPopuler,
                                  borderColor: v.isActive
                                    ? '#F24B5B'
                                    : '#929292',
                                }}
                                color={v.isActive ? '#F24B5B' : '#929292'}
                                text={Tema[i].StatusName}
                              />
                            </TouchableOpacity>

                            {/* <TouchableOpacity>
                          <Inter
                            style={styles.buttonVitamin}
                            text="Vitamin Sea"
                          />
                        </TouchableOpacity>

                        <TouchableOpacity>
                          <Inter
                            style={styles.buttonAlam}
                            text="Menyatu Dengan Alam"
                          />
                        </TouchableOpacity> */}
                            {/* <TouchableOpacity>
                              <Inter
                                style={styles.buttonNaik}
                                text="Naik Naik Ke Puncak Gunung"
                              />
                            </TouchableOpacity> */}
                          </View>
                        );
                      })}
                    </View>

                    <Inter style={styles.budgetTitle} text="Budget" />
                    <View style={styles.button3}>
                      <TextInput
                        style={styles.minim1}
                        placeholder="Minimum"
                        placeholderTextColor="#d8d8d8"
                        onChangeText={text => SetMinimum(text)}
                      />
                      <TextInput
                        style={styles.maks1}
                        placeholder="Maksimum"
                        placeholderTextColor="#d8d8d8"
                        onChangeText={text => SetMaksimum(text)}
                      />
                    </View>

                    <View style={styles.budgetStyle}>
                      {Budget.map((v, i) => {
                        return (
                          <View style={styles.button4}>
                            <TouchableOpacity
                              onFocus={() => setColor(true)}
                              onBlur={() => setColor(false)}
                              onPress={() => {
                                SetBudget(prevState => {
                                  const newArray = prevState.map(value => {
                                    value.isActive = false;
                                    return value;
                                  });
                                  newArray[i].isActive = true;

                                  return [...newArray];
                                });
                                // dispatch(getBudget(v.value));
                                submitBudget();

                                SetSendData({
                                  ...senddata,
                                  tipe: Budget[i].StatusName,
                                });
                              }}>
                              <Inter
                                style={{
                                  ...styles.buttonNum1,
                                  borderColor: v.isActive
                                    ? '#F24B5B'
                                    : '#929292',
                                }}
                                color={v.isActive ? '#F24B5B' : '#929292'}
                                text={Budget[i].StatusName}
                              />
                            </TouchableOpacity>
                            {/* <TouchableOpacity>
                          <Inter style={styles.buttonNum2} text="8-10 jt" />
                        </TouchableOpacity> */}
                          </View>
                        );
                      })}
                    </View>

                    <View style={styles.dateContainer}>
                      <Inter
                        style={styles.periodeTitle}
                        text="Periode Keberangkatan"
                      />
                    </View>
                    <View>
                      <TextInput
                        placeholder="dd/mm/yyyy"
                        style={styles.clickCalender}
                        mode="outlined"
                        outlineColor="#8b8b8b"
                        onChangeText={text => SetTripDate(text)}
                        right={
                          <TextInput.Icon
                            name="magnify"
                            color={COLORS.softBlack}
                            onPress={() =>
                              dispatch(
                                getDateTrip(
                                  moment(tripDate, 'DD/MM/YYYY').format(
                                    'DD/MM/YYYY',
                                  ),
                                ),
                              )
                            }
                            // style={styles.calender}
                          />
                        }
                      />
                    </View>

                    {/* <View>
                      <TouchableOpacity >
                        <Inter style={styles.calender} text={date} />
                      </TouchableOpacity>
                    </View> */}

                    <View style={styles.durasiContainer}>
                      <View
                      // onPress={() => {
                      //   submitDuration();

                      //   SetSendData({
                      //     ...senddata,
                      //     tipe: Durations.StatusName,
                      //   });
                      // }}
                      >
                        <Inter
                          style={styles.durasiTitle}
                          // text={Durations.StatusName}
                          text="Duration"
                        />
                      </View>
                      <TouchableOpacity
                        onPress={() => {
                          submitDuration();

                          SetSendData({
                            ...senddata,
                            tipe: Durations.StatusName,
                          });
                        }}>
                        <Inter
                          style={styles.durasiIcon}
                          // text={Durations.StatusName}
                          text=">>"
                        />
                      </TouchableOpacity>
                    </View>

                    <View style={styles.button5}>
                      <TextInput
                        style={styles.minim2}
                        placeholder="Minimum"
                        placeholderTextColor="#d8d8d8"
                        onChangeText={text => SetMinimum(text)}
                      />
                      <TextInput
                        style={styles.maks2}
                        placeholder="Maksimum"
                        placeholderTextColor="#d8d8d8"
                        onChangeText={text => SetMaksimum(text)}
                      />
                    </View>
                  </ScrollView>
                </View>
              )}
            </ModalSwipeable>
            <View style={styles.button}>
              {listButton.map((e, i) => {
                return (
                  <View key={i}>
                    <TouchableOpacity
                      onPress={() =>
                        dispatch(
                          getOneMonth({
                            start: moment(new Date(), 'DD/MM/YYYY').format(
                              'DD/MM/YYYY',
                            ),
                            end: moment(
                              [
                                day.toString(),
                                month.toString(),
                                year.toString(),
                              ],
                              'DD/MM/YYYY',
                            ).format('DD/MM/YYYY'),
                          }),
                        )
                      }>
                      <HomeButton
                        filter={e}
                        style={styles.buttonList}
                        warna={'#9f9f9f'}
                        text={'#9f9f9f'}
                      />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          </ScrollView>

          {LoadingData ? (
            <LoadingAnimated />
          ) : (
            <ScrollView
              refreshControl={
                <RefreshControl refreshing={refres} onRefresh={Refresh} />
              }
              style={styles.scroll}>
              <View style={styles.card}>
                <FlatList
                  data={DataSearchTrip}
                  ListEmptyComponent={() => (
                    <View style={styles.errorScreen}>
                      <ErrorScreen judul="There Is No Data Trippers" />
                    </View>
                  )}
                  keyExtractor={(items, index) => `${index}-trip`}
                  renderItem={({item, index}) => {
                    return (
                      <View key={index} style={styles.cardContainer}>
                        <View style={styles.card1}>
                          <View>
                            <TouchableOpacity
                              onPress={() => dispatch(getDetail(item.id))}>
                              <FastImage
                                source={{
                                  uri: item.thumbnail_pict,
                                }}
                                style={styles.cardImage}
                              />
                            </TouchableOpacity>
                          </View>

                          <View style={styles.textContainer}>
                            <Inter style={styles.trip} text={item.trip_name} />

                            <Inter
                              style={styles.termss}
                              text={item.term_and_condition}
                              line={2}
                            />
                            <View style={styles.textContainer2}>
                              <Inter style={styles.prices} text={item.price} />
                              <Inter
                                style={styles.durations}
                                text={`${item.duration_trip}Days`}
                              />
                            </View>

                            <View style={styles.textContainer3}>
                              <Feather
                                name="map-pin"
                                size={11}
                                color="#F24B5B"
                                style={styles.pinLoc}
                              />
                              <Inter
                                style={styles.locations}
                                text={item.pick_spot}
                              />
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            </ScrollView>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Trip;
