import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {
  getSearchTripFailed,
  GET_TRIP,
  GET_ONE_MONTH,
  GET_TEMA,
  GET_BUDGET,
  GET_DURATION,
  GET_SEARCH_NAME,
  GET_DATE_TRIP,
} from './ActionSearchTrip';
import {setSearchTrip, setDateTrip} from './ActionSearchTrip';
import moment from 'moment';

import {actionFailed, actionLoading} from '../../../Redux/GlobalAction';

function* sagaGetSearchTrip() {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      'https://fp-open-trip.herokuapp.com/api/ot/trip/all?page=0&limit=10',
    );
    console.log(res, 'SEARCHTRIP CEK DULU');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.result));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetOneMonth(action) {
  try {
    const {start, end} = action.payload;
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/date/search?start=${start}&end=${end}&page=0&limit=5`,
    );
    // console.log(res, 'SEARCHTRIP CEK DULU');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.result[0]));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetTema(action) {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/category/search?page=0&limit=5&cat1=${action.payload}`,
    );
    console.log(res, 'GET TEMA');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.results[0].Trips));
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetBudget(action) {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/budget/search?min=${action.payload.min}&max=${action.payload.max}&page=0&limit=2`,
    );
    console.log(res, 'GET BUDGET DEWI');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.result));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetDuration(action) {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/duration/search?minim=${action.payload.min}&maxim=${action.payload.max}&page=0&limit=2`,
    );
    console.log(res, 'GET DURATION DEWI');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.result));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetSearchName(action) {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/name/search?trip_name=${action.payload}&page=0&limit=5`,
    );
    console.log(res, 'GET SEARCHBAR DEWI');

    if (res.status === 200) {
      yield put(setSearchTrip(res.data.result));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaGetDateTrip(action) {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/date/search?start=${action.payload}&end=${action.payload}&page=0&limit=9`,
    );
    console.log(res, 'GET DATE TRIP DEWI');

    if (res.status === 200) {
      yield put(setDateTrip(res.data.result));
      yield put(actionLoading(false));
    } else {
      yield put(actionFailed(true));
      yield put(getSearchTripFailed(res));

      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getSearchTripFailed(error.response.data.message));

    yield put(actionLoading(false));
  }
}

function* sagaSearchTrip() {
  yield takeLatest(GET_TRIP, sagaGetSearchTrip);
  yield takeLatest(GET_ONE_MONTH, sagaGetOneMonth);
  yield takeLatest(GET_TEMA, sagaGetTema);
  yield takeLatest(GET_BUDGET, sagaGetBudget);
  yield takeLatest(GET_DURATION, sagaGetDuration);
  yield takeLatest(GET_SEARCH_NAME, sagaGetSearchName);
  yield takeLatest(GET_DATE_TRIP, sagaGetDateTrip);
}

export default sagaSearchTrip;
