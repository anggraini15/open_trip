import {GET_TRIP_FAILED, SET_TRIP, SET_DATE_TRIP} from './ActionSearchTrip';

const initialState = {
  data: [],
  message: '',
};

const reducerSearchTrip = (state = initialState, action) => {
  switch (action.type) {
    case SET_TRIP:
      return {
        ...state,
        data: action.payload,
      };
    case SET_DATE_TRIP:
      return {
        ...state,
        data: action.payload,
      };
    case GET_TRIP_FAILED:
      return {
        ...state,
        message: action.payload,
        data: null,
      };

    default:
      return state;
  }
};

export default reducerSearchTrip;
