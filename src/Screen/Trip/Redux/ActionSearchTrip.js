import moment from 'moment';

export const GET_TRIP = 'GET_TRIP';
export const GET_ONE_MONTH = 'GET_ONE_MONTH';
export const GET_TEMA = 'GET_TEMA';
export const GET_BUDGET = 'GET_BUDGET';
export const GET_DURATION = 'GET_DURATION';
export const GET_SEARCH_NAME = 'GET_SEARCH_NAME';
export const GET_DATE_TRIP = 'GET_DATE_TRIP';
export const SET_DATE_TRIP = 'SET_DATE_TRIP';

export const SET_TRIP = 'SET_TRIP';
export const GET_TRIP_FAILED = 'GET_TRIP_FAILED';

export const setSearchTrip = payload => {
  return {
    type: SET_TRIP,
    payload,
  };
};

export const setDateTrip = payload => {
  return {
    type: SET_DATE_TRIP,
    payload,
  };
};

export const getSearchTrip = payload => {
  return {
    type: GET_TRIP,
    payload,
  };
};

export const getOneMonth = payload => {
  return {
    type: 'GET_ONE_MONTH',
    payload,
  };
};

export const getTema = payload => {
  return {
    type: 'GET_TEMA',
    payload,
  };
};

export const getBudget = payload => {
  return {
    type: 'GET_BUDGET',
    payload,
  };
};

export const getDuration = payload => {
  return {
    type: 'GET_DURATION',
    payload,
  };
};

export const getSearchName = payload => {
  return {
    type: 'GET_SEARCH_NAME',
    payload,
  };
};

export const getDateTrip = payload => {
  return {
    type: 'GET_DATE_TRIP',
    payload,
  };
};

export const getSearchTripFailed = payload => ({
  type: GET_TRIP_FAILED,
  payload,
});
