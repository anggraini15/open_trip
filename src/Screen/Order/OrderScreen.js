import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  SafeAreaView,
  ScrollView,
  LogBox,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import HeaderTab from '../../Components/Header';
import Inter from '../../Components/Inter';
import {COLORS} from '../../Utils/Color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Snackbar} from 'react-native-paper';
import AuthButton from '../../Components/AuthButton';
import DropDown from 'react-native-dropdown-picker';
import {useDispatch, useSelector} from 'react-redux';
import {setOrderFailed, postOrder} from './Redux/ActionOrder';
import moment from 'moment';
import LoadingAnimated from '../../Components/Loading';
import {
  actionLoading,
  actionExpired,
  actionIsLogged,
  actionFailed,
  actionSuccess,
} from '../../Redux/GlobalAction';
import {navigate} from '../../Function/nav';
import {loginFailed} from '../Login/Redux/ActionLogin';

const OrderScreen = props => {
  const dispatch = useDispatch();

  const dataOrder = props.route.params;
  console.log(dataOrder);

  const [jumlahPeserta, setjumlahPeserta] = useState([1, 2]);
  const [selectedBank, setSelectedBank] = useState();

  const dataBank = useSelector(state => state.Order?.bank);
  const userdata = useSelector(state => state.Login?.userData);
  const dataDetail = useSelector(state => state.DetailHome.detail?.tripData);
  const token = useSelector(state => state.Login.data.token_traveller);
  const Loading = useSelector(state => state.Global.loading);
  const failed = useSelector(state => state.Global.failed);
  const success = useSelector(state => state.Global.Success);
  const message = useSelector(state => state.Order.message);
  const TimeExp = useSelector(state => state.Global.timeExp);

  const [open, setOpen] = useState(false);
  const [listBank, setlistBank] = useState(dataBank);
  console.log(listBank);
  const [items, setItems] = useState([
    {label: 'BRI', value: 'bri'},
    {label: 'BNI', value: 'bni'},
    {label: 'MANDIRI', value: 'mandiri'},
    {label: 'BCA', value: 'bca'},
    {label: 'BTN', value: 'btn'},
    {label: 'MUAMALAT', value: 'muamalat'},
  ]);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    if (moment().format() >= TimeExp) {
      dispatch(actionExpired(true));
      dispatch(actionIsLogged(false));
      dispatch(actionIsLogged(false));
      dispatch(loginFailed('Token Expired'));
      dispatch(actionFailed(true));
      navigate('LoginScreen');
    }
    dispatch(actionLoading(false));
  }, []);

  return (
    <SafeAreaView style={styles.safeView}>
      {Loading ? (
        <LoadingAnimated />
      ) : (
        <ScrollView contentContainerStyle={styles.scrollView}>
          <HeaderTab
            title="Your Order"
            arrow
            page={() => props.navigation.goBack()}
          />
          <View style={{paddingHorizontal: moderateScale(10)}}>
            {/* Paket Wisata */}
            <View style={styles.containerPrice}>
              <View style={{maxWidth: widthPercentageToDP(80)}}>
                <Inter
                  text={dataDetail.trip_name}
                  type="Bold"
                  size={moderateScale(16)}
                />
              </View>

              <View style={styles.boxDuration}>
                <Inter
                  text={`${dataDetail.duration_trip}Days`}
                  color="white"
                  size={moderateScale(12)}
                />
              </View>
            </View>

            {/* Duration */}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                maxWidth: widthPercentageToDP(65),
                justifyContent: 'space-between',
                marginVertical: moderateScale(30),
              }}>
              <View>
                <Inter
                  text={moment(dataOrder.date).format('dddd')}
                  size={moderateScale(14)}
                />
                <Inter
                  text={moment(dataOrder.date).format('DD MMMM')}
                  size={moderateScale(16)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>
              <Ionicons
                color={COLORS.black}
                size={moderateScale(20)}
                name="arrow-forward"
              />
              <View>
                <Inter
                  text={moment(dataOrder.date)
                    .add(dataDetail.duration_trip, 'days')
                    .format('dddd')}
                  size={moderateScale(14)}
                />
                <Inter
                  text={moment(dataOrder.date)
                    .add(dataDetail.duration_trip, 'days')
                    .format('DD MMMM')}
                  size={moderateScale(16)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>
            </View>

            {/* total harga & peserta */}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderTopWidth: moderateScale(1),
                borderBottomWidth: moderateScale(1),
                borderBottomColor: COLORS.softBlack,
                borderTopColor: COLORS.softBlack,
                paddingVertical: moderateScale(30),
              }}>
              <View>
                <Inter text="Total Harga" size={moderateScale(12)} />
                <Inter
                  text={`Rp. ${new Intl.NumberFormat(['ban', 'id']).format(
                    dataDetail.price * dataOrder.jumlahOrang,
                  )}`}
                  size={moderateScale(16)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>

              <View>
                <Inter text="Peserta" size={moderateScale(12)} />
                <Inter
                  text={`${dataOrder.jumlahOrang} Orang`}
                  size={moderateScale(16)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>
            </View>

            {/* data pemesan */}
            <View style={{marginVertical: moderateScale(30)}}>
              <Inter
                text="Data Pemesan"
                size={moderateScale(14)}
                color={COLORS.red}
              />
              <View style={{paddingVertical: moderateScale(10)}}>
                <View style={{paddingVertical: moderateScale(2)}}>
                  <Inter
                    text={userdata.username}
                    type="Bold"
                    size={moderateScale(12)}
                  />
                </View>
                <View style={{paddingVertical: moderateScale(2)}}>
                  <Inter text={userdata.email} size={moderateScale(12)} />
                </View>
                {/* <View style={{paddingVertical: moderateScale(2)}}>
                <Inter text="0877665544" size={moderateScale(12)} />
              </View> */}
              </View>
            </View>

            {/* detail peserta */}
            <View>
              {/* <Inter
              text="Detail Peserta"
              size={moderateScale(14)}
              color={COLORS.red}
            />
            <View>
              <FlatList
                data={jumlahPeserta}
                initialNumToRender={jumlahPeserta.length}
                keyExtractor={i => `${i}`}
                renderItem={({item, index}) => (
                  <View
                    style={{paddingVertical: moderateScale(5)}}
                    key={index.toString()}>
                    <Inter size={moderateScale(12)} text={`Peserta ${item}`} />
                    <TextInput
                      style={{
                        backgroundColor: COLORS.white,
                        paddingVertical: moderateScale(5),
                        borderRadius: moderateScale(5),
                      }}
                      outlineColor={COLORS.softBlack}
                      mode="outlined"
                      placeholder="Nama Peserta"
                    />
                    <TextInput
                      style={{
                        backgroundColor: COLORS.white,
                        paddingVertical: moderateScale(5),
                      }}
                      outlineColor={COLORS.softBlack}
                      mode="outlined"
                      placeholder="Email"
                    />
                    <TextInput
                      style={{
                        backgroundColor: COLORS.white,
                        paddingVertical: moderateScale(5),
                      }}
                      outlineColor={COLORS.softBlack}
                      mode="outlined"
                      placeholder="No. Handphone"
                    />
                  </View>
                )}
              />
            </View> */}

              {/* <AuthButton mini judul="Simpan" /> */}

              {/* Metode Pembayaran */}

              <View style={{paddingVertical: moderateScale(10)}}>
                <Inter
                  text="Metode Pembayaran"
                  size={moderateScale(14)}
                  color={COLORS.red}
                />
                <View style={{paddingVertical: moderateScale(10)}}>
                  <Inter
                    style={{textAlign: 'justify'}}
                    size={moderateScale(10)}
                    text="Payment using transfer via ATM, m-Banking or cash deposit. Payments are made to an Open Trip Virtual Account with the payment gateway NICEPAY. You will be automatically verified after making a payment. Your order will be active for 2 hours before it is automatically canceled by our system."
                  />
                </View>
                <View style={{paddingVertical: moderateScale(10)}}>
                  <Inter text="Select Payment Bank:" size={moderateScale(12)} />
                  {/* <Picker
                  itemStyle={{
                    borderWidth: 1,
                    borderColor: COLORS.softBlack,
                    fontFamily: 'Inter',
                    fontSize: moderateScale(12),
                    backgroundColor: COLORS.softBlack,
                  }}
                  style={{
                    elevation: 1,
                    borderWidth: moderateScale(5),
                    borderColor: COLORS.softBlack,
                    backgroundColor: COLORS.black,
                  }}
                  selectedValue={selectedBank}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedBank(itemValue)
                  }
                  mode="dropdown">
                  <Picker.Item
                    label="BRI"
                    value="bri"
                    style={{
                      backgroundColor: COLORS.softBlack,
                    }}
                  />
                  <Picker.Item label="BNI" value="bni" />
                  <Picker.Item label="BCA" value="bca" />
                  <Picker.Item label="MANDIRI" value="mandiri" />
                  <Picker.Item label="BANK JATIM" value="bankjatim" />
                </Picker> */}

                  <DropDown
                    schema={{
                      label: 'bank_name',
                      value: 'id',
                    }}
                    open={open}
                    value={selectedBank}
                    items={dataBank}
                    setOpen={setOpen}
                    setValue={setSelectedBank}
                    setItems={setlistBank}
                    mode="SIMPLE"
                    listMode="SCROLLVIEW"
                    ArrowDownIconComponent={() => (
                      <Ionicons
                        name="caret-down"
                        size={moderateScale(14)}
                        color={COLORS.red}
                      />
                    )}
                    ArrowUpIconComponent={() => (
                      <Ionicons
                        name="caret-up"
                        size={moderateScale(14)}
                        color={COLORS.red}
                      />
                    )}
                    showTickIcon={false}
                    style={{
                      borderColor: COLORS.softBlack,
                    }}
                    containerStyle={{
                      paddingVertical: moderateScale(20),
                    }}
                    ListEmptyComponent={() => <Inter text="select bank" />}
                  />

                  <Inter
                    style={{textAlign: 'justify'}}
                    size={moderateScale(10)}
                    text="Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy."
                  />
                </View>
                <View style={{paddingVertical: moderateScale(30), zIndex: 0}}>
                  <AuthButton
                    full
                    judul="Bayar"
                    submit={() => {
                      dispatch(
                        postOrder({
                          fullname: userdata.username,
                          email: userdata.email,
                          trip_id: dataDetail.id,
                          qty: dataOrder.jumlahOrang,
                          bank_id: selectedBank,
                          token,
                        }),
                      );
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      )}
      <Snackbar
        style={{backgroundColor: COLORS.red}}
        visible={failed}
        onDismiss={() => {
          dispatch(actionFailed(false));
          dispatch(actionSuccess(false));
          dispatch(setOrderFailed(''));
        }}
        duration={2000}>
        {message}
      </Snackbar>
    </SafeAreaView>
  );
};

export default OrderScreen;

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
  },
  scrollView: {
    flexGrow: 1,
  },
  containerPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  boxDuration: {
    backgroundColor: COLORS.black,
    width: widthPercentageToDP(12),
    alignItems: 'center',
  },
});
