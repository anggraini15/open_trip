export const GET_DATA_BANK = 'GET_DATA_BANK';
export const SET_DATA_BANK_SUCCESS = 'SET_DATA_BANK_SUCCESS';
export const SET_DATA_BANK_FAILED = 'SET_DATA_BANK_FAILED';
export const POST_ORDER = 'POST_ORDER';
export const SET_ORDER_SUCCESS = 'SET_ORDER_SUCCESS';
export const SET_ORDER_FAILED = 'SET_ORDER_FAILED';
export const GET_ORDER_ID = 'GET_ORDER_ID';

export const getDataBank = () => ({type: GET_DATA_BANK});
export const setDataBankSuccess = payload => ({
  type: SET_DATA_BANK_SUCCESS,
  payload,
});
export const setDataBankFailed = payload => ({
  type: SET_DATA_BANK_FAILED,
  payload,
});

export const postOrder = payload => ({type: POST_ORDER, payload});
export const setOrderSuccess = payload => ({type: SET_ORDER_SUCCESS, payload});
export const setOrderFailed = payload => ({type: SET_ORDER_FAILED, payload});

export const getOrderId = payload => ({type: GET_ORDER_ID, payload});
